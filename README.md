# adblockplus.org website

adblockplus.org is a [eyeo/websites/cms](https://gitlab.com/eyeo/websites/cms) website.

## Usage

See [eyeo/websites/cms](https://gitlab.com/eyeo/websites/cms) to learn how to run the website locally for development or build it for production.

You'll also have to `npm install` and `npm run build` to re-build static assets locally whenever you change them in development.

You can test `.htaccess` locally using apache2 and the provided adblockplus.org.conf virtual host.
